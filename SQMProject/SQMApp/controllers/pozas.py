from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Poza, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def pozas(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    print(e)
    if e and e.planificacion.user == request.user:
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Id", "Nombre", "Operación"]+years]
        laux = [{'data': "pid", 'readOnly': "true"}, {'data': "name"}, {'data': "op", 'type': "checkbox"}]
        for i in years:
            laux.append({'data': "op" + str(i), 'type': "checkbox"})
        l.append(laux)

        for poza in e.poza_set.all():
            auxdict = {'pid': poza.id, 'name': poza.nombre, 'op': bool(poza.operacion)}
            for i in range(0,len(years)):
                if i < len(poza.operacion_anual):
                    auxdict['op' + str(years[i])] = bool(poza.operacion_anual[i])
                else:
                    auxdict['op' + str(years[i])] = False
            l.append(auxdict)

        if len(l) == 2:
            d = {'pid': None, 'name': "", 'op': False}
            for i in years:
                d['op' + str(i)] = False
            l.append(d)
        print(l)
        context = {"planificacion_id": planificacion_id, "escenario_id": escenario_id, "pozas": json.dumps(l)}
        return render(request, 'SQMApp/pozas.html', context)

def save_table_pozas(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        response_data = dict()
        count = 1
        nulls = []
        errors = []

        e = Escenario.objects.get(id = escenario_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        attrs = ['name']
        print("lkasjdlkasjdlkasjd")
        not_delete = []
        clean = []
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    ops = []
                    for i in years:
                        try:
                            ops.append(bool(record['op' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                    print("trololololololololo")
                    if record['pid'] == None:
                        poza = Poza(
                            escenario = Escenario.objects.get(id = escenario_id),
                            nombre = record['name'],
                            operacion = bool(record['op']),
                            operacion_anual = ops)
                        poza.save()
                        print("Futuro id: ", poza.id)
                        record['pid'] = poza.id
                        not_delete.append(poza.id)
                    else:
                        poza = Poza.objects.get(id = int(record['pid']))
                        poza.nombre = record['name']
                        poza.operacion = bool(record['op'])
                        poza.operacion_anual = ops
                        not_delete.append(poza.id)
                        poza.save()
                except:
                    errors.append(count)

            else:
                if record['pid'] != None:
                    clean.append(count)
                nulls.append(count)
            count+=1

        Poza.objects.filter(escenario = Escenario.objects.get(id = escenario_id)).exclude(id__in = not_delete).delete()
        errorsAndNulls(response_data, nulls, errors)

        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Id", "Nombre", "Operación"]+years]
        laux = [{'data': "pid", 'readOnly': "true"}, {'data': "name"}, {'data': "op", 'type': "checkbox"}]
        for i in years:
            laux.append({'data': "op" + str(i), 'type': "checkbox"})
        l.append(laux)

        deleted = 0
        for i in clean:
            print("asd")
            del j_table[i-1-deleted]
            print("asd")
            deleted += 1

        response_data["data"] = l + j_table
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
