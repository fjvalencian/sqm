from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Purga, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def purgas(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        l = [["Nombre", "Unidad", "Nitrato", "Potasio", "Mínimo"]]
        for purga in e.purga_set.all():
            l.append({'name': purga.nombre, 'unit': purga.unidad,
                        'nit': float(purga.nitrato), 'pot': float(purga.potasio),
                        'min': float(purga.porc_minimo_pozas)})
        if len(l) == 1:
            l.append({'name': "", 'unit': "", 'nit': "", 'pot': "",
                        'min': ""})
        print(l)
        context = {"escenario_id": escenario_id, "purgas": json.dumps(l)}
        return render(request, 'SQMApp/purgas.html', context)

def save_table_purgas(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        Purga.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id)).delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        attrs = ['name', 'unit', 'nit', 'pot', 'min']
        print("lkasjdlkasjdlkasjd")
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    getcontext().prec = 3
                    purga = Purga(
                        escenario = Escenario.objects.get(id = escenario_id),
                        nombre = record['name'],
                        unidad = record['unit'],
                        nitrato = Decimal(record['nit']),
                        potasio = Decimal(record['pot']),
                        porc_minimo_pozas = Decimal(record['min'])
                        )
                    purga.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        errorsAndNulls(response_data, nulls, errors)

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
