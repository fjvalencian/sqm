from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import FuenteAgua, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def productos(request, planificacion_id, escenario_id):
    context = {}
    return render(request, 'SQMApp/productos.html', context)