from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

def checkNoneAndEmpty(record, attributes):
    for at in attributes:
        if record[at] == "" or record[at] == None:
            return True
    return False

def errorsAndNulls(response_data, nulls, errors):
    if len(nulls) > 0:
        s1 = "Columnas no guardadas por valores nulos:"
        for null in nulls:
            s1 += " " + str(null) + ","
        s1 = s1[:len(s1)-1]
        s1 += "."
        response_data["nulos"] = s1

    if len(errors) > 0:
        s2 = "Columnas no guardadas por errores insesperados:"
        for error in errors:
            s2 += " " + str(error) + ","
        s2 = s2[:len(s2)-1]
        s2 += "."
        response_data["errores"] = s2

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/SQM/')
            else:
                return HttpResponse("Your Rango account is disabled.")
        else:
            return HttpResponse("Invalid login details supplied.")

    else:
        return render(request, 'SQMApp/login.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/SQM/')
