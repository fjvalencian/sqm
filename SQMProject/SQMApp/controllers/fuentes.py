from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import FuenteAgua, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def fuentes(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    print(e)
    if e and e.planificacion.user == request.user:
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        laux = [{'data': "name"}]
        plantas = e.planta_set.all().order_by('id')
        faenas = e.faena_set.all().order_by('id')
        plantas_string = []
        faenas_string = []
        for p in plantas:
            laux.append({'data': "pl" + str(p.id), 'type': "checkbox"})
            plantas_string.append(str(p.id)+" - "+str(p.nombre))
        for f in faenas:
            laux.append({'data': "fa" + str(f.id), 'type': "checkbox"})
            faenas_string.append(str(f.id)+" - "+str(f.nombre))
        for i in years:
            laux.append({'data': "cap" + str(i), 'type': "checkbox"})
        l = [["Nombre"] + plantas_string + faenas_string +years]
        l.append(laux)

        for fuente in e.fuenteagua_set.all():
            auxdict = {'fuid': fuente.id, 'name': fuente.nombre}
            for p in plantas:
                if len(fuente.plantas.all().filter(id = p.id)) > 0:
                    auxdict['pl'+str(p.id)] = True
                else:
                    auxdict['pl'+str(p.id)] = False
            for f in faenas:
                if len(fuente.faenas.all().filter(id = f.id)) > 0:
                    auxdict['fa'+str(f.id)] = True
                else:
                    auxdict['fa'+str(f.id)] = False
            for i in range(0,len(years)):
                if i < len(fuente.capacidad_anual):
                    auxdict['cap' + str(years[i])] = bool(fuente.capacidad_anual[i])
                else:
                    auxdict['cap' + str(years[i])] = False
            l.append(auxdict)

        if len(l) == 2:
            d = {'fuid': None, 'name': "", 'cap': False}
            for p in plantas:
                d['pl'+str(p.id)] = False
            for f in faenas:
                d['fa'+str(f.id)] = False
            for i in years:
                d['cap' + str(i)] = False
            l.append(d)
        print(l)
        context = {"planificacion_id": planificacion_id, "escenario_id": escenario_id, "fuentes": json.dumps(l)}
        return render(request, 'SQMApp/fuentes.html', context)

def save_table_fuentes(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        response_data = dict()
        count = 1
        nulls = []
        errors = []

        e = Escenario.objects.get(id = escenario_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        plantas = e.planta_set.all().order_by('id')
        faenas = e.faena_set.all().order_by('id')

        attrs = ['name']
        print("lkasjdlkasjdlkasjd")
        not_delete = []
        clean = []
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    cap = []
                    for i in years:
                        try:
                            cap.append(bool(record['cap' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                    if record['fuid'] == None:
                        fuente = FuenteAgua(
                            escenario = Escenario.objects.get(id = escenario_id),
                            nombre = record['name'],
                            capacidad_anual = cap)
                        fuente.save()
                        for p in plantas:
                            if bool(record['pl'+str(p.id)]):
                                fuente.plantas.add(p)
                        for f in faenas:
                            if bool(record['fa'+str(f.id)]):
                                fuente.faenas.add(f)
                        fuente.save()
                        print("Futuro id: ", fuente.id)
                        record['fuid'] = fuente.id
                        not_delete.append(fuente.id)
                    else:
                        fuente = FuenteAgua.objects.get(id = int(record['fuid']))
                        fuente.nombre = record['name']
                        fuente.capacidad_anual = cap
                        not_delete.append(fuente.id)
                        fuente.save()
                        for p in plantas:
                            if bool(record['pl'+str(p.id)]):
                                if len(fuente.plantas.all().filter(id = p.id)) == 0:
                                    fuente.plantas.add(p)
                            else:
                                if len(fuente.plantas.all().filter(id = p.id)) > 0:
                                    fuente.plantas.remove(p)
                        for f in faenas:
                            if bool(record['fa'+str(f.id)]):
                                if len(fuente.faenas.all().filter(id = f.id)) == 0:
                                    fuente.faenas.add(f)
                            else:
                                if len(fuente.faenas.all().filter(id = f.id)) > 0:
                                    fuente.faenas.remove(f)
                        fuente.save()
                except:
                    errors.append(count)



            else:
                if record['fuid'] != None:
                    clean.append(count)
                nulls.append(count)
            count+=1

        FuenteAgua.objects.filter(escenario = Escenario.objects.get(id = escenario_id)).exclude(id__in = not_delete).delete()
        errorsAndNulls(response_data, nulls, errors)

        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        plantas_string = []
        faenas_string = []
        laux = [{'data': "name"}]
        for p in plantas:
            laux.append({'data': "pl" + str(p.id), 'type': "checkbox"})
            plantas_string.append(str(p.id)+" - "+str(p.nombre))
        for f in faenas:
            laux.append({'data': "fa" + str(f.id), 'type': "checkbox"})
            faenas_string.append(str(f.id)+" - "+str(f.nombre))
        for i in years:
            laux.append({'data': "cap" + str(i), 'type': "checkbox"})
        l = [["Nombre"] + plantas_string + faenas_string + years]

        l.append(laux)

        deleted = 0
        for i in clean:
            del j_table[i-1-deleted]
            deleted += 1

        response_data["data"] = l + j_table
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
