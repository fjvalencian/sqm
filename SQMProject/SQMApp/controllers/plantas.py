from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Planta, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def plantas(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    print(e)

    if e and e.planificacion.user == request.user:
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Id", "Nombre","Ver Planta","Operación"]+years]

        laux = [{'data': "plid", 'readOnly': "true"},
                {'data': "name"},
                {'data':"button",'renderer':"html",'readOnly':"false"},
                {'data': "op", 'type': "checkbox"},
                ]

        for i in years:
            laux.append({'data': "op" + str(i), 'type': "checkbox"})
        l.append(laux)

        for planta in e.planta_set.all():
            auxdict = {'plid': planta.id, 'name': planta.nombre ,'op': bool(planta.operacion)}
            auxdict['button'] = "<a href='/SQM/planificacion/" + str(e.planificacion.id) + "/escenario/" + str(e.id) + "/planta/" + str(planta.id) + "'><button class='btn btn-primary planif'>Ver Planta</button></a>"
            for i in range(0,len(years)):
                if i < len(planta.operacion_anual):
                    auxdict['op' + str(years[i])] = bool(planta.operacion_anual[i])
                else:
                    auxdict['op' + str(years[i])] = False
            l.append(auxdict)

        if len(l) == 2:
            d = {'plid': None, 'name': "",'button':"", 'op': False}
            for i in years:
                d['op' + str(i)] = False
            l.append(d)
        print(l)
        context = {"planificacion_id": planificacion_id, "escenario_id": escenario_id, "plantas": json.dumps(l)}
        return render(request, 'SQMApp/plantas.html', context)

def save_table_plantas(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        response_data = dict()
        count = 1
        nulls = []
        errors = []

        e = Escenario.objects.get(id = escenario_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        attrs = ['name']
        print("lkasjdlkasjdlkasjd")
        not_delete = []
        clean = []
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    ops = []
                    for i in years:
                        try:
                            ops.append(bool(record['op' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                    print("trololololololololo")
                    if record['plid'] == None:
                        planta = Planta(
                            escenario = Escenario.objects.get(id = escenario_id),
                            nombre = record['name'],
                            operacion = bool(record['op']),
                            operacion_anual = ops,
                            funcionamiento_minimo = [],
                            capacidad_anual = [])
                        planta.save()
                        print("Futuro id: ", planta.id)
                        record['plid'] = planta.id
                        record['button'] = "<a href='/SQM/planificacion/" + str(e.planificacion.id) + "/escenario/" + str(e.id) + "/planta/" + str(planta.id) + "'><button class='btn btn-primary planif'>Ver Planta</button></a>"

                        not_delete.append(planta.id)
                    else:
                        planta = Planta.objects.get(id = int(record['plid']))
                        planta.nombre = record['name']
                        planta.operacion = bool(record['op'])
                        operacion_anual = ops
                        not_delete.append(planta.id)
                        planta.save()
                except:
                    errors.append(count)

            else:
                if record['plid'] != None:
                    clean.append(count)
                nulls.append(count)
            count+=1

        Planta.objects.filter(escenario = Escenario.objects.get(id = escenario_id)).exclude(id__in = not_delete).delete()
        errorsAndNulls(response_data, nulls, errors)

        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Id", "Nombre","button","Operación"]+years]
        laux = [{'data': "plid", 'readOnly': "true"},
                {'data': "name"},
                {'data':"button",'renderer':"html",'readOnly':"false"},
                {'data': "op", 'type': "checkbox"},
                ]
        for i in years:
            laux.append({'data': "op" + str(i), 'type': "checkbox"})
        l.append(laux)

        deleted = 0
        for i in clean:
            del j_table[i-1-deleted]
            deleted += 1

        response_data["data"] = l + j_table
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
