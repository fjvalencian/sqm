from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import ProductoComercial, ProductoIntermedio, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def productos_comerciales(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    print(e)
    if e and e.planificacion.user == request.user:
        pis = ProductoIntermedio.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id))
        prod_inter = [pi.nombre for pi in pis]
        print(prod_inter)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Producto Intermedio", "País", "Mercado"]+4*years]
        laux = [{'data': "pi", 'type': 'dropdown', 'source': prod_inter}, {'data': "country"},
            {'data': "merc"}]
        for i in years:
            laux.append({'data': "mv" + str(i)})
        for i in years:
            laux.append({'data': "ret" + str(i)})
        for i in years:
            laux.append({'data': "dem" + str(i)})
        for i in years:
            laux.append({'data': "demmin" + str(i)})
        l.append(laux)

        for productocomercial in e.productocomercial_set.all():
            auxdict = {'pi': productocomercial.producto_intermedio.nombre,
                        'country': productocomercial.pais,
                        'merc': productocomercial.mercado}
            for i in range(0,len(years)):
                if i < len(productocomercial.meses_venta):
                    auxdict['mv' + str(years[i])] = int(productocomercial.meses_venta[i])
                else:
                    auxdict['mv' + str(years[i])] = 0
            for i in range(0,len(years)):
                if i < len(productocomercial.retornos):
                    auxdict['ret' + str(years[i])] = float(productocomercial.retornos[i])
                else:
                    auxdict['ret' + str(years[i])] = 0
            for i in range(0,len(years)):
                if i < len(productocomercial.demanda):
                    auxdict['dem' + str(years[i])] = float(productocomercial.demanda[i])
                else:
                    auxdict['dem' + str(years[i])] = 0
            for i in range(0,len(years)):
                if i < len(productocomercial.demanda_minima):
                    auxdict['demmin' + str(years[i])] = float(productocomercial.demanda_minima[i])
                else:
                    auxdict['demmin' + str(years[i])] = 0
            l.append(auxdict)
        if len(l) == 2:
            d = {'pi': None, 'country': "", 'merc': ""}
            for i in years:
                d['mv' + str(i)] = ""
            for i in years:
                d['ret' + str(i)] = ""
            for i in years:
                d['dem' + str(i)] = ""
            for i in years:
                d['demmin' + str(i)] = ""
            l.append(d)
        print(l)
        context = {"escenario_id": escenario_id, "planificacion_id":planificacion_id ,"productoscomerciales": json.dumps(l)}
        return render(request, 'SQMApp/productoscomerciales.html', context)

def save_table_productos_comerciales(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        ProductoComercial.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id)).delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []

        e = Escenario.objects.get(id = escenario_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        attrs = ['pi', 'country', 'merc']
        print("lkasjdlkasjdlkasjd")
        for record in j_table:
            pi = ProductoIntermedio.objects.all().filter(nombre = record['pi'])
            if(not(checkNoneAndEmpty(record, attrs)) and len(pi)>0):
                pi = pi[0]
                try:
                    getcontext().prec = 3

                    mvs = []
                    rets = []
                    dems = []
                    demmins = []
                    for i in years:
                        try:
                            mv.append(int(record['mv' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                        try:
                            rets.append(Decimal(record['ret' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                        try:
                            dems.append(Decimal(record['dem' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                        try:
                            demmins.append(Decimal(record['demmin' + str(i)]))
                        except:
                            print("quedó la cagá hno")

                    print("trololololololololo")
                    pc = ProductoComercial(
                        escenario = Escenario.objects.get(id = escenario_id),
                        producto_intermedio = pi,
                        pais = record['country'],
                        mercado = record['merc'],
                        meses_venta = mvs,
                        retornos = rets,
                        demanda = dems,
                        demanda_minima = demmins)
                    pc.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        errorsAndNulls(response_data, nulls, errors)

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
