from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Consumo, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
import json
from .utils import *

@login_required
def consumos(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        l = [["Nombre", "Unidad"]]
        for c in e.consumo_set.all():
            l.append({'name': c.nombre, 'unit': c.unidad})
        if len(l) == 1:
            l.append({'name': "", 'unit': ""})
        print(l)
        context = {"escenario_id": escenario_id, "consumos": json.dumps(l)}
        return render(request, 'SQMApp/consumos.html', context)

def save_table_consumos(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        Consumo.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id)).delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        attrs = ['name','unit']
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    cons = Consumo(
                        escenario = Escenario.objects.get(id = escenario_id),
                        nombre = record['name'],
                        unidad = record['unit'],
                        )
                    cons.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        errorsAndNulls(response_data, nulls, errors)

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
