from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Planificacion
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
import json
from .utils import *

@login_required
def planificaciones(request):
    plans = Planificacion.objects.all()
    l = [["Id", "Fav", "Nombre", "Año Inicio", "Año término", "Comentarios", "Planificaciones"]]
    for p in plans:
        l.append({'pid': p.id, 'fav': p.star, 'name': p.nombre,
        'iyear': p.ano_inicio, 'fyear': p.ano_fin, 'comments': p.comentarios,
        'button': '<a href="/SQM/planificacion/'+str(p.id)+'/escenarios"><button class="btn btn-primary planif"> Entrar </button></a>'})
    if len(l) == 1:
        l.append({'pid': None , 'fav': False, 'name': "",
        'iyear': 0, 'fyear': 0, 'comments': "", 'button': ""})
    context = {"plans": json.dumps(l)}
    print(context)
    return render(request, 'SQMApp/planificaciones.html', context)

def save_table_planificaciones(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        print(j_table)
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        attrs = ['name','iyear', 'fyear', 'comments']
        not_delete = []
        clean = []
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):

                try:
                    if record['pid'] == None:
                        plan = Planificacion(
                            user = request.user,
                            star = bool(record['fav']),
                            nombre = record['name'],
                            ano_inicio = int(record['iyear']),
                            ano_fin = int(record['fyear']),
                            comentarios = record['comments'],
                            fecha_ultima_modificacion = timezone.now(),
                            tags = []
                            )
                        plan.save()

                        print("Futuro id: ", plan.id)
                        if record['fav'] == None:
                            record['fav'] = False
                        record['pid'] = plan.id
                        record['button'] = '<a href="/SQM/planificacion/'+str(plan.id)+'/escenarios"><button class="btn btn-primary planif"> Entrar </button></a>'
                        not_delete.append(plan.id)

                    else:
                        plan = Planificacion.objects.get(id = int(record['pid']))
                        plan.star = bool(record['fav'])
                        plan.nombre = record['name']
                        plan.ano_inicio = int(record['iyear'])
                        plan.ano_fin = int(record['fyear'])
                        plan.comentarios = record['comments']
                        plan.fecha_ultima_modificacion = timezone.now()
                        plan.save()
                        tags = []
                        not_delete.append(plan.id)
                except:
                    errors.append(count)

            else:
                if record['pid'] != None:
                    clean.append(count)
                nulls.append(count)
            count+=1
        try:
            Planificacion.objects.filter(user = request.user).exclude(id__in = not_delete).delete()
        except:
            print("Error borrando")

        errorsAndNulls(response_data, nulls, errors)

        deleted = 0
        for i in clean:
            del j_table[i-1-deleted]
            deleted += 1

        j_table = ([["Id", "Fav", "Nombre", "Año Inicio",
        "Año término", "Comentarios", "Planificaciones"]] + j_table)

        response_data["data"] = j_table
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
