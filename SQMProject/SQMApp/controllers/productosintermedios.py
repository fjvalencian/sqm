from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import ProductoIntermedio, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def productos_intermedios(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    conv_unit = {'T': 'Toneladas', 'm': 'M. cúbicos'}
    print(e)
    if e and e.planificacion.user == request.user:
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Nombre", "Unidad", "Nitrato", "Potasio", "Inv. Inicial", "Tipo"]+3*years]
        laux = [{'data': "name"},
            {'data': "unit", 'type': "dropdown", 'source': ["Toneladas", "M. cúbicos"]},
            {'data': "nit"},
            {'data': "pot"}, {'data': "invinit"},
            {'data': "tipo", 'type': "dropdown", 'source': ["Yodo", "Nitrato", "Nada"]}]
        for i in years:
            laux.append({'data': "im" + str(i)})
        for i in years:
            laux.append({'data': "pd" + str(i)})
        for i in years:
            laux.append({'data': "imin" + str(i)})
        l.append(laux)

        for productointermedio in e.productointermedio_set.all():
            auxdict = {'name': productointermedio.nombre, 'unit': conv_unit[productointermedio.unidad],
                        'nit': float(productointermedio.nitrato), 'pot': float(productointermedio.potasio),
                        'invinit': float(productointermedio.inventario_inicial), 'tipo': productointermedio.tipo}
            for i in range(0,len(years)):
                if i < len(productointermedio.inventario_maximo):
                    auxdict['im' + str(years[i])] = float(productointermedio.inventario_maximo[i])
                else:
                    auxdict['im' + str(years[i])] = 0
            for i in range(0,len(years)):
                if i < len(productointermedio.produccion_anterior_desfasada):
                    auxdict['pd' + str(years[i])] = float(productointermedio.produccion_anterior_desfasada[i])
                else:
                    auxdict['pd' + str(years[i])] = 0
            for i in range(0,len(years)):
                if i < len(productointermedio.inventario_minimo):
                    auxdict['imin' + str(years[i])] = float(productointermedio.inventario_minimo[i])
                else:
                    auxdict['imin' + str(years[i])] = 0
            l.append(auxdict)
        if len(l) == 2:
            d = {'name': "", 'unit': "", 'nit': "", 'pot': "", 'invinit': "", 'tipo': ""}
            for i in years:
                d['im' + str(i)] = ""
            for i in years:
                d['pd' + str(i)] = ""
            for i in years:
                d['imin' + str(i)] = ""
            l.append(d)
        print(l)
        context = {"escenario_id": escenario_id, "productosintermedios": json.dumps(l)}
        return render(request, 'SQMApp/productosintermedios.html', context)

def save_table_productos_intermedios(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        ProductoIntermedio.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id)).delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []

        e = Escenario.objects.get(id = escenario_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))

        attrs = ['name', 'unit', 'nit', 'pot', 'invinit', 'tipo']
        conv_unit = {'Toneladas': 'T', 'M. cúbicos': 'm'}
        print("lkasjdlkasjdlkasjd")
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    getcontext().prec = 3

                    inv_max = []
                    prod_d = []
                    inv_min = []
                    for i in years:
                        try:
                            inv_max.append(Decimal(record['im' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                        try:
                            prod_d.append(Decimal(record['pd' + str(i)]))
                        except:
                            print("quedó la cagá hno")
                        try:
                            inv_min.append(Decimal(record['imin' + str(i)]))
                        except:
                            print("quedó la cagá hno")

                    print("trololololololololo")
                    pi = ProductoIntermedio(
                        escenario = Escenario.objects.get(id = escenario_id),
                        nombre = record['name'],
                        unidad = conv_unit[record['unit']],
                        nitrato = Decimal(record['nit']),
                        potasio = Decimal(record['pot']),
                        inventario_inicial = Decimal(record['invinit']),
                        tipo = record['tipo'],
                        produccion_anterior_desfasada = prod_d,
                        inventario_maximo = inv_max,
                        inventario_minimo = inv_min)
                    pi.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        errorsAndNulls(response_data, nulls, errors)

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
