from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Faena, Faena_Proceso, CostosOperacionDiaria, Escenario, Consumo
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

def getCapFM(request, planificacion_id, escenario_id, faena_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        mi_faena = Faena.objects.get(id = faena_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [years]
        laux = []
        for i in years:
            laux.append({'data': "y" + str(i), 'type': "numeric"})
        l.append(laux)

        cap_an = mi_faena.capacidad_anual
        cap_j = {}
        if len(cap_an) == len(years):
            for i in range(len(years)):
                cap_j['y'+str(years[i])] = float(cap_an[i])
        else:
            for i in years:
                cap_j['y'+str(i)] = 0

        fm = mi_faena.funcionamiento_minimo
        cap_j2 = {}
        if len(fm) == len(years):
            for i in range(len(years)):
                cap_j2['y'+str(years[i])] = float(fm[i])
        else:
            for i in years:
                cap_j2['y'+str(i)] = 0

        l.append(cap_j)
        l.append(cap_j2)
        return l

def getConsumos(request, planificacion_id, escenario_id, faena_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        mi_faena = Faena.objects.get(id = faena_id)
        consumos = e.consumo_set.all()
        source_consumos = list(c.nombre for c in consumos)
        l = [["Nombre"]]
        l.append([{'data': 'name', 'type': 'dropdown', 'source': source_consumos}])
        consumos_faena = mi_faena.consumos.all()
        for c in consumos_faena:
            l.append({'cid': c.id, 'name': c.nombre})
        if len(l) == 1:
            l.append({'name': ""})
        return l

def getCostos(request, planificacion_id, escenario_id, faena_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        mi_faena = Faena.objects.get(id = faena_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Nombre"]+years]
        laux = [{'data': "name"}]
        for i in years:
            laux.append({'data': "y" + str(i), 'type': "numeric"})
        l.append(laux)

        costos = mi_faena.costos_operacion_diaria.all()
        if len(costos) > 0:
            for costo in costos:
                j_costo = dict()
                j_costo['cid'] = costo.id
                j_costo['name'] =  costo.nombre
                print(len(years))
                print(len(costo.anual))
                if len(years) == len(costo.anual):
                    for i in range(len(years)):
                        j_costo['y' + str(years[i])] = float(costo.anual[i])
                else:
                    for i in years:
                        j_costo['y' + str(i)] = 0
                l.append(j_costo)
        else:
            j_costo = dict()
            j_costo['name'] = ""
            for i in years:
                j_costo['y' + str(i)] = 0
        return l

def getProc(request, planificacion_id, escenario_id, faena_id):
    e = Escenario.objects.get(id = escenario_id)
    if e and e.planificacion.user == request.user:
        mi_faena = Faena.objects.get(id = faena_id)
        years = list(range(e.planificacion.ano_inicio, e.planificacion.ano_fin + 1))
        l = [["Nombre", "Operación"]+years]
        laux = [{'data': "name"}, {'data': 'op', 'type': 'checkbox'}]
        for i in years:
            laux.append({'data': "y" + str(i), 'type': "checkbox"})
        l.append(laux)

        procesos = mi_faena.procesos.all()
        if len(procesos) > 0:
            for proceso in procesos:
                j_proceso = dict()
                j_proceso['prid'] = proceso.id
                j_proceso['name'] =  proceso.nombre
                j_proceso['op'] = proceso.operacion
                if len(years) == len(proceso.operacion_anual):
                    for i in range(len(years)):
                        j_proceso['y' + str(years[i])] = bool(proceso.operacion_anual[i])
                else:
                    for i in years:
                        j_proceso['y' + str(i)] = False
                l.append(j_proceso)
        else:
            j_proceso = dict()
            j_proceso['name'] = ""
            j_proceso['op'] = False
            for i in years:
                j_proceso['y' + str(i)] = False
            l.append(j_proceso)
        print("ELE: ", str(l))
        return l

@login_required
def faena(request, planificacion_id, escenario_id, faena_id):
    l = getCapFM(request, planificacion_id, escenario_id, faena_id)
    print(l)
    l2 = getConsumos(request, planificacion_id, escenario_id, faena_id)
    l3 = getCostos(request, planificacion_id, escenario_id, faena_id)
    l4 = getProc(request, planificacion_id, escenario_id, faena_id)
    context = {"planificacion_id": planificacion_id, "escenario_id": escenario_id, "faena_id": faena_id,
                "cafm": json.dumps(l),
                "con": json.dumps(l2), "cos": json.dumps(l3), "pro": json.dumps(l4)}
    return render(request, 'SQMApp/faena.html', context)

def save_table_consumos(request):
    j_table = json.loads(request.POST.get('table_json2'))
    escenario_id = request.POST.get('escenario_id')
    faena_id = request.POST.get('faena_id')
    count = 1
    nulls = []
    errors = []

    e = Escenario.objects.get(id = escenario_id)
    faena = Faena.objects.get(id = faena_id)
    faena.consumos.clear()

    attrs = ['name']
    not_delete = []
    clean = []

    for record in j_table:
        if('name' in record.keys()):
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    print("trololololololololo")
                    consumo = Consumo.objects.get(nombre = record['name'], escenario = e)
                    print("Consumo: ", consumo)
                    consumo.save()
                    faena.consumos.add(consumo)
                    faena.save()
                except:
                    print("Error")
                count+=1

def save_table_costos(request):
    j_table = json.loads(request.POST.get('table_json3'))
    escenario_id = request.POST.get('escenario_id')
    faena_id = request.POST.get('faena_id')
    count = 1
    nulls = []
    errors = []

    e = Escenario.objects.get(id = escenario_id)
    p = e.planificacion
    ai = p.ano_inicio
    af = p.ano_fin
    faena = Faena.objects.get(id = faena_id)

    attrs = ['name']
    not_delete = []
    clean = []

    for record in j_table:
        print("RECORD: ", record)
        if('name' in record.keys()):
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    new = False
                    if 'cid' not in record.keys():
                        new = True
                    else:
                        if record['cid'] == None:
                            new = True
                    anos = []
                    for i in range(int(ai), int(af) + 1):
                        anos.append(record['y' + str(i)])
                    if new:
                        costo = CostosOperacionDiaria(nombre = record['name'], anual = anos)
                        costo.save()
                        not_delete.append(costo.id)
                        faena.costos_operacion_diaria.add(costo)
                        faena.save()
                    else:
                        print("lakjdslkasjdlkasdjklasjkldkljasd")
                        print(record['cid'])
                        costo = CostosOperacionDiaria.objects.get(id = int(record['cid']))
                        print("lakjdslkasjdlkasdjklasjkldkljasd2")
                        costo.nombre = record['name']
                        costo.anual = anos
                        not_delete.append(costo.id)
                        costo.save()
                except:
                    print("Error")
                count+=1
    print("Not delete: ", str(not_delete))
    faena.costos_operacion_diaria.all().exclude(id__in = not_delete).delete()

def save_table_proc(request):
    j_table = json.loads(request.POST.get('table_json4'))
    escenario_id = request.POST.get('escenario_id')
    faena_id = request.POST.get('faena_id')
    count = 1
    nulls = []
    errors = []

    e = Escenario.objects.get(id = escenario_id)
    p = e.planificacion
    ai = p.ano_inicio
    af = p.ano_fin
    faena = Faena.objects.get(id = faena_id)

    attrs = ['name']
    not_delete = []
    clean = []

    for record in j_table:
        if('name' in record.keys()):
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    new = False
                    if 'prid' not in record.keys():
                        new = True
                    else:
                        if record['prid'] == None:
                            new = True
                    anos = []
                    for i in range(int(ai), int(af) + 1):
                        anos.append(bool(record['y' + str(i)]))
                    if new:
                        pf = Faena_Proceso(nombre = record['name'], operacion_anual = anos,
                                operacion = bool(record['op']))
                        pf.save()
                        faena.procesos.add(pf)
                        not_delete.append(pf.id)
                        faena.save()
                    else:
                        pf = Faena_Proceso.objects.get(id = int(record['prid']))
                        pf.nombre = record['name']
                        pf.operacion_anual = anos
                        pf.operacion = bool(record['op'])
                        not_delete.append(pf.id)
                        pf.save()
                except:
                    print("Error")
                count+=1

    print("Not Delete PROC: ", str(not_delete))
    faena.procesos.all().exclude(id__in = not_delete).delete()

def save_table_cap(request):
    j_table = json.loads(request.POST.get('table_json'))
    escenario_id = request.POST.get('escenario_id')
    faena_id = request.POST.get('faena_id')
    print('FAENA ID: ', faena_id)
    count = 1
    nulls = []
    errors = []

    e = Escenario.objects.get(id = escenario_id)
    p = e.planificacion
    ai = p.ano_inicio
    af = p.ano_fin
    faena = Faena.objects.get(id = faena_id)

    attrs = []
    not_delete = []


    a1 = []
    a2 = []
    for i in range(int(ai), int(af) + 1):
        a1.append(j_table[0]['y' + str(i)])
        a2.append(j_table[1]['y' + str(i)])
    faena.capacidad_anual = a1
    faena.funcionamiento_minimo = a2
    faena.save()


    count+=1



def save_table_faena(request):
    if request.method == 'POST':
        response_data = dict()
        save_table_cap(request)
        save_table_consumos(request)
        save_table_costos(request)
        save_table_proc(request)
        eid = request.POST.get('escenario_id')
        escenario = Escenario.objects.get(id = eid)
        pid = escenario.planificacion.id
        response_data['redirect'] = ("/SQM/planificacion/" + str(pid) +
                                    "/escenario/" + str(eid) + "/faena/" + str(request.POST.get('faena_id')))
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
