from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import Escenario, Planificacion
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
import json
from .utils import *

@login_required
def escenarios(request, planificacion_id):
    p = Planificacion.objects.get(id = planificacion_id)
    if p and p.user == request.user:
        a_inicio = p.ano_inicio
        a_fin = p.ano_fin
        comentario = p.comentarios
        f_ultima_modificacion = p.fecha_ultima_modificacion
        l = [["Id", "Nombre", "Año", "Tasa", "Comentario", "Ir"]]

        for e in p.escenario_set.all():
            l.append({
                'eid': e.id, 
                'name': e.nombre, 
                'ano': e.anos_perpetuidad,
                'tasa': float(e.tasa_descuento), 
                'com': e.comentarios,
                'button': '<a href="/SQM/planificacion/'+str(planificacion_id)+'/escenario/'+str(e.id)+'/productos"><button class="btn btn-primary planif"> Entrar </button></a>'})
        
        if len(l) == 1:
            l.append({'eid': None, 'name': "", 'ano': 0, 'tasa': 0, 'com': "", 'button':''})
        print(l)
        context = {"planificacion_id": planificacion_id, "escenarios": json.dumps(l)}
        context['ai'] = a_inicio
        context['af'] = a_fin
        context['comentario'] = comentario
        context['fecha_ultima_mod'] = f_ultima_modificacion

        return render(request, 'SQMApp/escenarios.html', context)

def save_table_escenarios(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        planificacion_id = request.POST.get('plan_id')
        print(j_table)
        print("Planificacion id: ", planificacion_id)
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        not_delete = []
        attrs = ['name','ano', 'tasa', 'com']
        clean = []
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    if record['eid'] == None:
                        esc = Escenario(
                            planificacion = Planificacion.objects.get(id = planificacion_id),
                            nombre = record['name'],
                            anos_perpetuidad = int(record['ano']),
                            tasa_descuento = float(record['tasa']),
                            comentarios = record['com'],
                            fecha_ultima_modificacion = timezone.now(),
                            optimizacion = "sv"
                            )
                        esc.save()
                        print("Futuro id: ", esc.id)
                        record['eid'] = esc.id
                        record['button'] = '<a href="/SQM/planificacion/'+str(planificacion_id)+'/escenario/'+str(esc.id)+'/productos"><button class="btn btn-primary planif"> Entrar </button></a>'
                        not_delete.append(esc.id)
                    else:
                        esc = Escenario.objects.get(id = int(record['eid']))
                        esc.nombre = record['name']
                        esc.anos_perpetuidad = int(record['ano'])
                        esc.tasa_descuento = float(record['tasa'])
                        esc.comentarios = record['com']
                        esc.fecha_ultima_modificacion = timezone.now()
                        esc.save()
                        not_delete.append(esc.id)
                except:
                    errors.append(count)
            else:
                if record['eid'] != None:
                    clean.append(count)
                nulls.append(count)
            count+=1
        try:
            Escenario.objects.filter(planificacion = Planificacion.objects.get(id = planificacion_id)).exclude(id__in = not_delete).delete()
        except:
            print("Error borrando")
        errorsAndNulls(response_data, nulls, errors)
        deleted = 0
        for i in clean:
            del j_table[i-1-deleted]
            deleted += 1

        j_table = ([["Id", "Nombre", "Año",
        "Tasa", "Comentario","Ir"]] + j_table)
        response_data["data"] = j_table
        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
