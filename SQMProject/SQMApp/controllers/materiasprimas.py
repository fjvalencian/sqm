from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from SQMApp.models import MateriaPrima, Escenario
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.shortcuts import render
from decimal import *
import json
from .utils import *

@login_required
def materias_primas(request, planificacion_id, escenario_id):
    e = Escenario.objects.get(id = escenario_id)
    conv_unit = {'T': 'Toneladas', 'm': 'M. cúbicos'}
    print(e)
    if e and e.planificacion.user == request.user:
        l = [["Nombre", "Unidad", "Nitrato", "Potasio"]]
        for materiaprima in e.materiaprima_set.all():
            l.append({'name': materiaprima.nombre, 'unit': conv_unit[materiaprima.unidad],
                        'nit': float(materiaprima.nitrato), 'pot': float(materiaprima.potasio)})
        if len(l) == 1:
            l.append({'name': "", 'unit': "", 'nit': "", 'pot': ""})
        print(l)
        context = {"escenario_id": escenario_id, "materiasprimas": json.dumps(l)}
        return render(request, 'SQMApp/materiasprimas.html', context)

def save_table_materias_primas(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        escenario_id = request.POST.get('escenario_id')
        print(j_table)
        print("Escenario id: ", escenario_id)
        MateriaPrima.objects.all().filter(escenario = Escenario.objects.get(id = escenario_id)).delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        attrs = ['name', 'unit', 'nit', 'pot']
        conv_unit = {'Toneladas': 'T', 'M. cúbicos': 'm'}
        print("lkasjdlkasjdlkasjd")
        for record in j_table:
            if(not(checkNoneAndEmpty(record, attrs))):
                try:
                    getcontext().prec = 3
                    mp = MateriaPrima(
                        escenario = Escenario.objects.get(id = escenario_id),
                        nombre = record['name'],
                        unidad = conv_unit[record['unit']],
                        nitrato = Decimal(record['nit']),
                        potasio = Decimal(record['pot'])
                        )
                    mp.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        errorsAndNulls(response_data, nulls, errors)

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
