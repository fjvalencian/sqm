from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.template.context_processors import csrf
# from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import User
from .models import Post
import json

def index(request):
    posts = Post.objects.all()
    l = [["Select", "Post", "Author", "Likes"]]
    for p in posts:
        l.append({'select': False, 'post': p.post_text, 'author': p.post_author, 'likes': p.post_likes})
    if len(l) == 1:
        l.append({'select': False, 'post': "", 'author': "", 'likes': ""})
    context = {"posts": json.dumps(l)}
    return render(request, 'SQMApp/index.html', context)


def save_table(request):
    if request.method == 'POST':
        j_table = json.loads(request.POST.get('table_json'))
        print(j_table)
        Post.objects.all().delete()
        response_data = dict()
        count = 1
        nulls = []
        errors = []
        for record in j_table:
            if(record["post"] != "" and record["author"] != "" and record["likes"] != "" and
                record["post"] != None and record["author"] != None and record["likes"] != None):
                try:
                    p = Post(post_text = record["post"], post_author = record["author"],  post_likes = int(record["likes"]))
                    p.save()
                except:
                    errors.append(count)
            else:
                nulls.append(count)
            count+=1

        if len(nulls) > 0:
            s1 = "Columnas no guardadas por valores nulos:"
            for null in nulls:
                s1 += " " + str(null) + ","
            s1 = s1[:len(s1)-1]
            s1 += "."
            response_data["nulos"] = s1

        if len(errors) > 0:
            s2 = "Columnas no guardadas por errores insesperados:"
            for error in errors:
                s2 += " " + str(error) + ","
            s2 = s2[:len(s2)-1]
            s2 += "."
            response_data["errores"] = s2

        return HttpResponse(
                            json.dumps(response_data),
                            content_type="application/json"
                        )
    return HttpResponseRedirect('/SQM/')
