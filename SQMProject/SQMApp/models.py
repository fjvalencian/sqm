#============================================================
#clear terminal
#============================================================
import os

#============================================================

from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User

UNIDADES=(
    ("T","T"),
    ("m","m3")
)
ESTADOS_OPTIMIZACION=(
    ("op","Optimizado"),
    ("se","Sin errores"),
    ("ev","Errores de validación"),
    ("eo","Errores de optimización"),
    ("sv","Sin validar")
)

class Post(models.Model):
    post_text = models.TextField()
    post_likes = models.BigIntegerField()
    post_author = models.TextField()





class Planificacion(models.Model):
    user = models.ForeignKey(User)
    star = models.BooleanField()
    nombre = models.CharField(max_length=100)
    ano_inicio = models.PositiveIntegerField()
    ano_fin = models.PositiveIntegerField()
    comentarios = models.TextField()
    fecha_ultima_modificacion = models.DateTimeField(auto_now=True)
    tags = ArrayField(models.CharField(max_length=50))

    def __str__(self):
        return "Planificación " + str(self.id)

class Escenario(models.Model):
    planificacion = models.ForeignKey("Planificacion")
    nombre = models.CharField(max_length=100)
    anos_perpetuidad = models.PositiveIntegerField()
    tasa_descuento = models.DecimalField(max_digits=5, decimal_places=3)
    comentarios = models.TextField()
    fecha_ultima_modificacion = models.DateTimeField(auto_now=True)
    optimizacion = models.CharField(max_length=2, choices=ESTADOS_OPTIMIZACION)
    compartidos = models.ManyToManyField(User)

    def __str__(self):
        return "Escenario " + str(self.id)

class ProductoIntermedio(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    unidad = models.CharField(max_length=1, choices=UNIDADES)
    inventario_inicial = models.DecimalField(max_digits=15, decimal_places=3)
    inventario_maximo = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    produccion_anterior_desfasada = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    inventario_minimo = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    nitrato = models.DecimalField(max_digits=15, decimal_places=3)
    potasio = models.DecimalField(max_digits=15, decimal_places=3)
    tipo = models.CharField(max_length=100)

class MateriaPrima(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    unidad = models.CharField(max_length=1, choices=UNIDADES)
    nitrato = models.DecimalField(max_digits=15, decimal_places=3)
    potasio = models.DecimalField(max_digits=15, decimal_places=3)

    def __str__(self):
        return "Materia Prima " + str(self.id)

class Purga(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    unidad = models.CharField(max_length=1, choices=UNIDADES)
    nitrato = models.DecimalField(max_digits=15, decimal_places=3)
    potasio = models.DecimalField(max_digits=15, decimal_places=3)
    porc_minimo_pozas = models.DecimalField(max_digits=5, decimal_places=3)

    def __str__(self):
        return "Purga " + str(self.id)

class Consumo(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    unidad = models.CharField(max_length=100)

    def __str__(self):
        return "Consumo " + str(self.id)

class ProductoComercial(models.Model):
    escenario = models.ForeignKey("Escenario")
    producto_intermedio = models.ForeignKey("ProductoIntermedio")
    pais = models.CharField(max_length=100)
    mercado = models.CharField(max_length=100)
    meses_venta = ArrayField(models.PositiveIntegerField())
    retornos = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    demanda = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    demanda_minima = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))

class Poza(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    operacion = models.BooleanField()
    operacion_anual = ArrayField(models.BooleanField())
    inputs = models.ManyToManyField("Poza_Input")
    outputs = models.ManyToManyField("Poza_Output")
    costos_variables = models.ManyToManyField("Proceso_CostoVariable")

class Poza_Input(models.Model):
    producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    materia_prima = models.ManyToManyField("MateriaPrima")
    purga = models.ManyToManyField("Purga")

class Poza_Output(models.Model):
    producto_intermedio = models.ManyToManyField("ProductoIntermedio", through="PO_PI")
    purga = models.ManyToManyField("Purga", through="PO_Purga")
    desfase = models.DecimalField(max_digits=15, decimal_places=3)
    perdidas_nitrato = models.DecimalField(max_digits=5, decimal_places=3)
    perdidas_potasio = models.DecimalField(max_digits=5, decimal_places=3)
    relacionado_nitrato = models.BooleanField()
    relacionado_potasio = models.BooleanField()
    relacionado_purgas = models.BooleanField()
    relacionado_materia_prima = models.ManyToManyField("MateriaPrima")

class Proceso_CostoVariable(models.Model):
    nombre = models.CharField(max_length=100)
    anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    relacionado_nitrato = models.BooleanField()
    relacionado_potasio = models.BooleanField()
    relacionado_purgas = models.BooleanField()
    relacionado_producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    relacionado_materia_prima = models.ManyToManyField("MateriaPrima")
    relacionado_purga = models.ManyToManyField("Purga")

class Planta(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    operacion = models.BooleanField()
    operacion_anual = ArrayField(models.BooleanField())
    capacidad_anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    funcionamiento_minimo = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    consumos = models.ManyToManyField("Consumo")
    costos_operacion_diaria = models.ManyToManyField("CostosOperacionDiaria")
    procesos = models.ManyToManyField("Planta_Proceso")

    def __str__(self):
        return "Planta "+str(self.id)

class CostosOperacionDiaria(models.Model):
    nombre = models.CharField(max_length=100)
    anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))

class Proceso_CapacidadDiaria(models.Model):
    nombre = models.CharField(max_length=100)
    anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    relacionado_nitrato = models.BooleanField()
    relacionado_potasio = models.BooleanField()
    relacionado_purgas = models.BooleanField()
    relacionado_producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    relacionado_materia_prima = models.ManyToManyField("MateriaPrima")
    relacionado_purga = models.ManyToManyField("Purga")

class Planta_Proceso(models.Model):
    nombre = models.CharField(max_length=100)
    operacion = models.BooleanField()
    operacion_anual = ArrayField(models.BooleanField())
    # inputs = models.ManyToManyField("Proceso_Input")
    inputs_purgas = models.ManyToManyField("Planta_InputPurga")
    inputs_purgas_nitrato = models.DecimalField(max_digits=15, decimal_places=3)
    inputs_purgas_potasio = models.DecimalField(max_digits=15, decimal_places=3)
    outputs = models.ManyToManyField("Planta_Output")
    capacidad_diaria = models.ManyToManyField("Proceso_CapacidadDiaria")
    costos_variables = models.ManyToManyField("Proceso_CostoVariable")
    consumos = models.ManyToManyField("Proceso_Consumo")

class Planta_InputPurga(models.Model):
    producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    materia_prima = models.ManyToManyField("MateriaPrima")
    purga = models.ManyToManyField("Purga")

class Proceso_Input(models.Model):
    proceso_planta = models.ForeignKey("Planta_Proceso")
    q = models.DecimalField(max_digits=15, decimal_places=3)
    producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    materia_prima = models.ManyToManyField("MateriaPrima")
    purga = models.ManyToManyField("Purga")

class Planta_Output(models.Model):
    desfase = models.DecimalField(max_digits=15, decimal_places=3)
    q = models.DecimalField(max_digits=15, decimal_places=3)
    producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    purga = models.ManyToManyField("Purga")

class Proceso_Consumo(models.Model):
    nombre = models.CharField(max_length=100)
    consumo = models.ForeignKey("Consumo")
    anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    relacionado_nitrato = models.BooleanField()
    relacionado_potasio = models.BooleanField()
    relacionado_purgas = models.BooleanField()
    relacionado_producto_intermedio = models.ManyToManyField("ProductoIntermedio")
    relacionado_materia_prima = models.ManyToManyField("MateriaPrima")
    relacionado_purga = models.ManyToManyField("Purga")

class Faena(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    operacion = models.BooleanField()
    operacion_anual = ArrayField(models.BooleanField())
    capacidad_anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    funcionamiento_minimo = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))
    consumos = models.ManyToManyField("Consumo")
    costos_operacion_diaria = models.ManyToManyField("CostosOperacionDiaria")
    procesos = models.ManyToManyField("Faena_Proceso")

class Faena_Proceso(models.Model):
    nombre = models.CharField(max_length=100)
    operacion = models.BooleanField()
    operacion_anual = ArrayField(models.BooleanField())
    inputs = models.ManyToManyField("Proceso_Input")
    outputs = models.ManyToManyField("Faena_Output")
    capacidad_diaria = models.ManyToManyField("Proceso_CapacidadDiaria")
    costos_variables = models.ManyToManyField("Proceso_CostoVariable")
    consumos = models.ManyToManyField("Proceso_Consumo")

class Faena_Output(models.Model):
    desfase = models.DecimalField(max_digits=15, decimal_places=3)
    q = models.DecimalField(max_digits=15, decimal_places=3)
    producto_intermedio = models.ManyToManyField("ProductoIntermedio", through='Faena_PI')
    purga = models.ManyToManyField("Purga", through='Faena_Purga')
    relacionado_materia_prima = models.ManyToManyField("MateriaPrima")
    eficiencia = ArrayField(ArrayField(models.DecimalField(max_digits=15, decimal_places=3)))

class Faena_Purga(models.Model):
    input_output = models.CharField(max_length=100)
    faena = models.ForeignKey("Faena_Output")
    purga = models.ForeignKey("Purga")

class Faena_PI(models.Model):
    input_output = models.CharField(max_length=100)
    faena = models.ForeignKey("Faena_Output")
    producto_intermedio = models.ForeignKey("ProductoIntermedio")

class PO_Purga(models.Model):
    input_output = models.CharField(max_length=100)
    poza_output = models.ForeignKey("Poza_Output")
    purga = models.ForeignKey("Purga")

class PO_PI(models.Model):
    input_output = models.CharField(max_length=100)
    poza_output = models.ForeignKey("Poza_Output")
    producto_intermedio = models.ForeignKey("ProductoIntermedio")

class FuenteAgua(models.Model):
    escenario = models.ForeignKey("Escenario")
    nombre = models.CharField(max_length=100)
    plantas = models.ManyToManyField("Planta")
    faenas = models.ManyToManyField("Faena")
    capacidad_anual = ArrayField(models.DecimalField(max_digits=15, decimal_places=3))

class Bloqueo(models.Model):
    user_bloqueador = models.ForeignKey(User)
    timestamp_bloqueo = models.DateTimeField()
    escenario_bloqueado = models.ForeignKey("Escenario")
