from django.conf.urls import url
from django.conf import settings
from . import views
import SQMApp.controllers as controllers

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^save_table/$', views.save_table, name='save_table'),
    url(r'^save_table_planificaciones/$', controllers.planificaciones.save_table_planificaciones, name='save_table_planificaciones'),
    url(r'^save_table_escenarios/$', controllers.escenario.save_table_escenarios, name='save_table_escenarios'),
    url(r'^save_table_consumos/$', controllers.consumos.save_table_consumos, name='save_table_consumos'),
    url(r'^save_table_purgas/$', controllers.purgas.save_table_purgas, name='save_table_purgas'),
    url(r'^save_table_materias_primas/$', controllers.materiasprimas.save_table_materias_primas, name='save_table_materias_primas'),
    url(r'^save_table_productos_intermedios/$', controllers.productosintermedios.save_table_productos_intermedios, name='save_table_productos_intermedios'),
    url(r'^save_table_productos_comerciales/$', controllers.productoscomerciales.save_table_productos_comerciales, name='save_table_productos_comerciales'),
    url(r'^save_table_plantas/$', controllers.plantas.save_table_plantas, name='save_table_plantas'),
    url(r'^save_table_faenas/$', controllers.faenas.save_table_faenas, name='save_table_faenas'),
    url(r'^save_table_fuentes/$', controllers.fuentes.save_table_fuentes, name='save_table_fuentes'),
    url(r'^save_table_planta/$', controllers.planta.save_table_planta, name='save_table_planta'),
    url(r'^save_table_faena/$', controllers.faena.save_table_faena, name='save_table_faena'),
    url(r'^save_table_pozas/$', controllers.pozas.save_table_pozas, name='save_table_pozas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenarios/$', controllers.escenario.escenarios, name='escenarios'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/consumos/$', controllers.consumos.consumos, name='consumos'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/purgas/$', controllers.purgas.purgas, name='purgas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/materias_primas/$', controllers.materiasprimas.materias_primas, name='materias_primas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/productos_intermedios/$', controllers.productosintermedios.productos_intermedios, name='productos_intermedios'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/productos_comerciales/$', controllers.productoscomerciales.productos_comerciales, name='productos_comerciales'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/plantas/$', controllers.plantas.plantas, name='plantas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/faenas/$', controllers.faenas.faenas, name='faenas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/pozas/$', controllers.pozas.pozas, name='pozas'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/fuentes/$', controllers.fuentes.fuentes, name='fuentes'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/planta/(?P<planta_id>[0-9]+)/$', controllers.planta.planta, name='planta'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/faena/(?P<faena_id>[0-9]+)/$', controllers.faena.faena, name='faena'),
    url(r'^planificacion/(?P<planificacion_id>[0-9]+)/escenario/(?P<escenario_id>[0-9]+)/productos/$', controllers.productos.productos, name='productos'),


    #falta: faenas,

    url(r'^planificaciones/$', controllers.planificaciones.planificaciones, name='planificaciones'),
    # url(r'^escenarios/$', views.escenarios, name='escenarios'),
    #url(r'^productos/$', views.productos, name='productos'),
    url(r'^login/$', controllers.utils.user_login, name='login'),
    url(r'^logout/$', controllers.utils.user_logout, name='logout'),
    #url(r'^escenario_p1e1/$', views.escenario_p1e1, name='escenario_p1e1'),
    # url(r'^productos_intermedios/$', views.productos_intermedios, name='productosintermedios'),
    # url(r'^productos_comerciales/$', views.productos_comerciales, name='productos_comerciales'),
    url(r'^pozas/$', controllers.pozas.pozas, name='pozas'),



]
