from django.contrib import admin
from .models import Post, Planificacion, Escenario, Consumo, Purga, MateriaPrima, ProductoIntermedio

admin.site.register(Post)
admin.site.register(Planificacion)
admin.site.register(Escenario)
admin.site.register(Consumo)
admin.site.register(Purga)
admin.site.register(MateriaPrima)
admin.site.register(ProductoIntermedio)
