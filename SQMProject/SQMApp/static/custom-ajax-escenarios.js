$(function() {

  function createTable(data, container) {
    return new Handsontable(container, {
      data: data.slice(1,data.length),
      minSpareRows: 1,
      rowHeaders: true,
      colHeaders: data[0],
      contextMenu: false,
      colWidths: [30, 100, 100, 100, 200,90],
      columns: [
        {
          data: 'eid',
          readOnly: 'true',
        },
        {
          data: 'name',
        },
        {
          data: 'ano',
          type: 'numeric'
        },
        {
          data: 'tasa',
        },
        {
          data: 'com'
        },
        {
          data: 'button',
          renderer: 'html',
          readOnly: 'true',

        },
      ]
    });
  }

  var data = $('#table-index').data('esc');
  console.log(data);
  var container = document.getElementById('table-index');
  var hot = createTable(data, container);


  var
    $$ = function(id) {
      return document.getElementById(id);
    },
    container = $$('table-index'),
    tableConsole = $$('table-console'),
    save = $$('saveTable'),
    autosaveNotification;

  Handsontable.Dom.addEvent(save, 'click', function() {
    // save all cell's data
    event.preventDefault();
    console.log("Going to ajax");
    $.ajax({
        url : "/SQM/save_table_escenarios/",
        type : "POST",
        data : { table_json: JSON.stringify(hot.getData()), plan_id: $('#saveTable').data('planificacion')},
        success : function(json) {
          if (json.errores && json.nulos) {
            $('#table-console').html(json.errores + "</BR>" + json.nulos);
            notificationNulo(json.errores, true);
          }
          else if (json.errores) {
            $('#table-console').html(json.errores);
            notificationNulo(json.errores, true);
          }
          else if (json.nulos) {
            $('#table-console').html(json.nulos);
            notificationNulo(json.errores, true);
          }
          else {
            $('#table-console').html("Guardado Exitoso");
          }
          $('#table-index').empty();
          hot = createTable(json.data, container);
          console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
  });

//toaster notification
    function notificationNulo(msj, gg){
      if(gg){
        toastr.error("Error!");
      }
      else{
        toastr.success(msj);
      }
        toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "1500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
  }
  // This function gets cookie with a given name
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }

  var csrftoken = getCookie('csrftoken');

  /*
  The functions below will create a header with csrftoken
  */

  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  function sameOrigin(url) {
      // test that a given url is a same-origin URL
      // url could be relative or scheme relative or absolute
      var host = document.location.host; // host + port
      var protocol = document.location.protocol;
      var sr_origin = '//' + host;
      var origin = protocol + sr_origin;
      // Allow absolute or scheme relative URLs to same origin
      return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
          (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
          // or any other URL that isn't scheme relative or absolute i.e relative.
          !(/^(\/\/|http:|https:).*/.test(url));
  }

  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
              // Send the token to same-origin, relative URLs only.
              // Send the token only if the method warrants CSRF protection
              // Using the CSRFToken value acquired earlier
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });

});
