$(function() {

  function createTableCapAnual(data, container) {
    return new Handsontable(container, {
      data: data.slice(2,data.length),
      rowHeaders: ['Cap', 'F. Min'],
      colHeaders: data[0],
      contextMenu: true,
      colWidths: [100, 100],

      columns: data[1]
    });
  }

  function createTable(data, container) {
    return new Handsontable(container, {
      data: data.slice(2,data.length),
      minSpareRows: 1,
      rowHeaders: true,
      colHeaders: data[0],
      contextMenu: true,
      columns: data[1],
    });
  }

  var data = $('#table-index').data('planta');
  var data2 = $('#table2-index').data('planta');
  var data3 = $('#table3-index').data('planta');
  var data4 = $('#table4-index').data('planta');
  console.log(data2);
  console.log(data4);
  var container = document.getElementById('table-index');
  var container2 = document.getElementById('table2-index');
  var container3 = document.getElementById('table3-index');
  var container4 = document.getElementById('table4-index');
  var hot = createTableCapAnual(data, container);
  var hot2 = createTable(data2, container2);
  var hot3 = createTable(data3, container3);
  var hot4 = createTable(data4, container4);


  var
    $$ = function(id) {
      return document.getElementById(id);
    },
    container = $$('table-index'),
    tableConsole = $$('table-console'),
    save = $$('saveTable'),
    autosaveNotification;

  Handsontable.Dom.addEvent(save, 'click', function() {
    // save all cell's data
    event.preventDefault();
    console.log("Going to ajax");
    $.ajax({
        url : "/SQM/save_table_planta/",
        type : "POST",
        data : { table_json: JSON.stringify(hot.getData()), escenario_id: $('#saveTable').data('escenario'),
                table_json2: JSON.stringify(hot2.getData()), table_json3: JSON.stringify(hot3.getData()),
                table_json4: JSON.stringify(hot4.getData()), planta_id: $('#saveTable').data('planta')},
        success : function(json) {
          window.location.href = json.redirect
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>");
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
  });


  // This function gets cookie with a given name
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }

  var csrftoken = getCookie('csrftoken');

  /*
  The functions below will create a header with csrftoken
  */

  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  function sameOrigin(url) {
      // test that a given url is a same-origin URL
      // url could be relative or scheme relative or absolute
      var host = document.location.host; // host + port
      var protocol = document.location.protocol;
      var sr_origin = '//' + host;
      var origin = protocol + sr_origin;
      // Allow absolute or scheme relative URLs to same origin
      return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
          (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
          // or any other URL that isn't scheme relative or absolute i.e relative.
          !(/^(\/\/|http:|https:).*/.test(url));
  }

  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
              // Send the token to same-origin, relative URLs only.
              // Send the token only if the method warrants CSRF protection
              // Using the CSRFToken value acquired earlier
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });

});
